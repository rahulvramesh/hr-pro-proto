<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	public function index()
	{

	}

  /*
  * Wizard To Create New Invoice Slip
  */
  public function generate()
	{

	}


  /*
  * Show Client List And Number Of Invoice Generated For Them
  * Once They Click On Client Redirect To `list_for` And Show List Of Invoices
  */
  public function list_all()
  {

  }

  /*
  * List All Invoice Genearted For Particular Clinent
  * Note : It Should Contain Buttons For Edit and Delete
  */
  public function list_for()
	{

	}

  /*
  * Show Invoice With id `$id`
  */
  public function view($id)
  {

  }



}
