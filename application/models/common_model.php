<?php
class Common_model extends CI_Model {

    function insert_data($data, $table) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function insert_batch_record($table, $data){
        $this->db->insert_batch($table, $data);
        return true;
    }

    function update_data_by_id($data, $id, $table) {
        $this->db->where('id', $id);
        $this->db->update($table, $data);
        return true;
    }

    function update_data_by_fields($data, $fields, $table) {
        foreach($fields as $key=>$value){
            $this->db->where($key, $value);
        }

        $this->db->update($table, $data);
    }

    function delete_data($id, $table){
        $this->db->where('id', $id);
        $this->db->delete($table);
        return true;
    }

    function delete_all_data($table){
        $this->db->delete($table);
    }

    function delete_row_by_field($field, $value, $table){
        $this->db->where($field, $value);
        $this->db->delete($table);
    }

    function delete_row_by_fields($fields, $table){
        foreach($fields as $key=>$value){
            $this->db->where($key, $value);
        }
        $this->db->delete($table);
    }

    function get_details_by_id($id, $table){
        $this->db->where('id', $id);
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row;
        } else {
            return false;
        }
    }


}
