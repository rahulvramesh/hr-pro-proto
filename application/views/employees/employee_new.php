<div class="wrapper">
            <div class="container">

                <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <!-- <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button> -->
                           <!--  <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul> -->
                        </div>
                        <h4 class="page-title">Employee Code :#1122 </h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">

                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">Employee Info</h4>

                            <div class="row">
                                <div class="col-lg-6">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">First Name</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="First Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" for="example-email">Email</label>
                                            <div class="col-md-10">
                                                <input type="email" id="example-email" name="example-email" class="form-control" placeholder="email@example.com">
                                            </div>
                                        </div>
                                        <div>
                                        <label class="col-md-2 control-label" for="example-email">Gender</label>
                                         <input type="radio" name="gender" value="male"> Male
                                        <input type="radio" name="gender" value="female"> Female<br>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Street 1</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="Street 1">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Street 2</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="Street 2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">City</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="City">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">State</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="State">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Zip code</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="Zip code">
                                            </div>
                                       <!--  </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="">
                                            </div> -->
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Description</label>
                                            <div class="col-md-10">
                                                <textarea class="form-control" rows="5" placeholder="More info.."></textarea>
                                            </div>
                                        </div>

                                    </form>

                                </div>

                                <!-- end col -->

                                 <div class="row">
                                <div class="col-lg-6">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Last Name</label>
                                            <div class="col-md-10">
                                                <input type="text" class="form-control" placeholder="Last Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Photo</label>
                                            <div class="col-md-10">
                                                <input type="file" >
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-sm-2 control-label">Stati control</label>
                                            <div class="col-sm-10">
                                              <p class="form-control-static">email@example.com</p>
                                            </div>
                                        </div> -->

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Years of Experiance</label>
                                            <div class="col-sm-10">
                                                <select class="form-control">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Mobile No</label>
                                            <div class="col-sm-10">
                                              <input type="text" class="form-control" placeholder="Mobile no">

                                            </div>

                                        </div>


                                    </form>
                                <!-- </div> --><!-- end col -->

                            </div><!-- end row -->
                        </div>

                    </div><!-- end col -->
<div align="center"><br><button type="button" class="btn btn-inverse btn-rounded w-md waves-effect waves-light m-b-5">Submit</button></div>
                </div>
                          </div>
                      </div>
                  </div>

            <!--   </div> -->
              <!-- end row -->


              <!-- Footer -->
              <footer class="footer text-right">
                  <div class="container">
                      <div class="row">
                          <div class="col-xs-6">
                              2016 © Alindal Technologies.
                          </div>
                          <div class="col-xs-6">
                              <ul class="pull-right list-inline m-b-0">
                                  <li>
                                      <a href="#">About</a>
                                  </li>
                                  <li>
                                      <a href="#">Help</a>
                                  </li>
                                  <li>
                                      <a href="#">Contact</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </footer>
              <!-- End Footer -->

          </div>
          <!-- end container -->



          <!-- Right Sidebar -->
          <div class="side-bar right-bar">
              <a href="javascript:void(0);" class="right-bar-toggle">
                  <i class="zmdi zmdi-close-circle-o"></i>
              </a>
              <h4 class="">Notifications</h4>
              <div class="notification-list nicescroll">
                  <ul class="list-group list-no-border user-list">
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="avatar">
                                  <img src="<?php echo base_url(); ?>assets/images/users/avatar-2.jpg" alt="">
                              </div>
                              <div class="user-desc">
                                  <span class="name">Michael Zenaty</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">2 hours ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-info">
                                  <i class="zmdi zmdi-account"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">New Signup</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">5 hours ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-pink">
                                  <i class="zmdi zmdi-comment"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">New Message received</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">1 day ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item active">
                          <a href="#" class="user-list-item">
                              <div class="avatar">
                                  <img src="<?php echo base_url(); ?>assets/images/users/avatar-3.jpg" alt="">
                              </div>
                              <div class="user-desc">
                                  <span class="name">James Anderson</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">2 days ago</span>
                              </div>
                          </a>
                      </li>
                      <li class="list-group-item active">
                          <a href="#" class="user-list-item">
                              <div class="icon bg-warning">
                                  <i class="zmdi zmdi-settings"></i>
                              </div>
                              <div class="user-desc">
                                  <span class="name">Settings</span>
                                  <span class="desc">There are new settings available</span>
                                  <span class="time">1 day ago</span>
                              </div>
                          </a>
                      </li>

                  </ul>
                  <!-- Footer -->
              <footer class="footer text-right">
                  <div class="container">
                      <div class="row">
                          <div class="col-xs-6">
                              2016 © Alindal Technologies.
                          </div>
                          <div class="col-xs-6">
                              <ul class="pull-right list-inline m-b-0">
                                  <li>
                                      <a href="#">About</a>
                                  </li>
                                  <li>
                                      <a href="#">Help</a>
                                  </li>
                                  <li>
                                      <a href="#">Contact</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </footer>
              <!-- End Footer -->

              </div>
          </div>
