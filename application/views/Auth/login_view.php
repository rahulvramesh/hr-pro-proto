<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App title -->
        <title>Alindal - HR Pro</title>

        <!-- App CSS -->
        <?php echo link_tag('assets/css/bootstrap.min.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/core.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/components.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/icons.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/pages.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/menu.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/responsive.css" rel="stylesheet" type="text/css');?>
        <?php echo isset($page_links) ? $page_links : '' ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script>var base_url = '<?php echo base_url() ?>';</script>
        <?php echo script_tag('assets/js/modernizr.min.js');?>


    </head>
    <body>

        <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
            <div class="text-center">
                <a href="index.html" class="logo"><span>Alindal<span>System</span></span></a>
                <h5 class="text-muted m-t-0 font-600">Industry Leading System</h5>
            </div>
        	<div class="m-t-40 card-box">
                <div class="text-center">
                    <h4 class="text-uppercase font-bold m-b-0">Sign In</h4>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal m-t-20" action="<?php echo base_url(); ?>/index.php/Dashboard">

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <input class="form-control" type="text" required="" placeholder="Username">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <input class="form-control" type="password" required="" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-xs-12">
                                <div class="checkbox checkbox-custom">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup">
                                        Remember me
                                    </label>
                                </div>

                            </div>
                        </div>

                        <div class="form-group text-center m-t-30">
                            <div class="col-xs-12">
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" type="submit">Log In</button>
                            </div>
                        </div>

                        <div class="form-group m-t-30 m-b-0">
                            <div class="col-sm-12">
                                <a href="page-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- end card-box-->



        </div>
        <!-- end wrapper page -->



    	<script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <?php echo script_tag('assets/js/jquery.min.js');?>
        <?php echo script_tag('assets/js/bootstrap.min.js');?>
        <?php echo script_tag('assets/js/detect.js');?>
        <?php echo script_tag('assets/js/fastclick.js');?>
        <?php echo script_tag('assets/js/jquery.slimscroll.js');?>
        <?php echo script_tag('assets/js/jquery.blockUI.js');?>
        <?php echo script_tag('assets/js/waves.js');?>
        <?php echo script_tag('assets/js/wow.min.js');?>
        <?php echo script_tag('assets/js/jquery.nicescroll.js');?>
        <?php echo script_tag('assets/js/jquery.scrollTo.min.js');?>

        <!-- App js -->
        <?php echo script_tag('assets/js/jquery.core.js');?>
        <?php echo script_tag('assets/js/jquery.app.js');?>

	</body>
</html>
