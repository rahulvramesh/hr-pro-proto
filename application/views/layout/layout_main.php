<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
        <meta name="author" content="Coderthemes">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">

        <!-- App title -->
        <title>Alindal - HR Pro</title>

        <!-- App CSS -->
        <?php echo link_tag('assets/css/bootstrap.min.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/core.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/components.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/icons.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/pages.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/menu.css" rel="stylesheet" type="text/css');?>
        <?php echo link_tag('assets/css/responsive.css" rel="stylesheet" type="text/css');?>
        <?php echo isset($page_links) ? $page_links : '' ?>

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script>var base_url = '<?php echo base_url() ?>';</script>
        <?php echo script_tag('assets/js/modernizr.min.js');?>


    </head>
    <body>

      <!-- Navigation Bar-->
      <header id="topnav">
          <div class="topbar-main">
              <div class="container">

                  <!-- LOGO -->
                  <div class="topbar-left">
                      <a href="index.html" class="logo"><span>Alindal<span>Pro</span></span></a>
                  </div>
                  <!-- End Logo container-->


                  <div class="menu-extras">

                      <ul class="nav navbar-nav navbar-right pull-right">
                          <li>
                              <form role="search" class="navbar-left app-search pull-left hidden-xs">
                                   <input type="text" placeholder="Search..." class="form-control">
                                   <a href=""><i class="fa fa-search"></i></a>
                              </form>
                          </li>
                          <li>
                              <!-- Notification -->
                              <div class="notification-box">
                                  <ul class="list-inline m-b-0">
                                      <li>
                                          <a href="javascript:void(0);" class="right-bar-toggle">
                                              <i class="zmdi zmdi-notifications-none"></i>
                                          </a>
                                          <div class="noti-dot">
                                              <span class="dot"></span>
                                              <span class="pulse"></span>
                                          </div>
                                      </li>
                                  </ul>
                              </div>
                              <!-- End Notification bar -->
                          </li>

                          <li class="dropdown user-box">
                              <a href="" class="dropdown-toggle waves-effect waves-light profile " data-toggle="dropdown" aria-expanded="true">
                                  <img src="<?php echo base_url(); ?>assets/images/users/avatar-1.jpg" alt="user-img" class="img-circle user-img">
                                  <div class="user-status away"><i class="zmdi zmdi-dot-circle"></i></div>
                              </a>

                              <ul class="dropdown-menu">
                                  <li><a href="javascript:void(0)"><i class="ti-user m-r-5"></i> Profile</a></li>
                                  <li><a href="javascript:void(0)"><i class="ti-settings m-r-5"></i> Settings</a></li>
                                  <li><a href="javascript:void(0)"><i class="ti-lock m-r-5"></i> Lock screen</a></li>
                                  <li><a href="javascript:void(0)"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                              </ul>
                          </li>
                      </ul>
                      <div class="menu-item">
                          <!-- Mobile menu toggle-->
                          <a class="navbar-toggle">
                              <div class="lines">
                                  <span></span>
                                  <span></span>
                                  <span></span>
                              </div>
                          </a>
                          <!-- End mobile menu toggle-->
                      </div>
                  </div>

              </div>
          </div>

          <div class="navbar-custom">
              <div class="container">
                  <div id="navigation">
                      <!-- Navigation Menu-->
                      <ul class="navigation-menu">
                          <li >
                              <a href="<?php echo base_url(); ?>index.php/Dashboard" class="active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                          </li>
                          <li class="has-submenu">
                              <a href="#"><i class="zmdi zmdi-invert-colors"></i> <span> Employees </span> </a>
                              <ul class="submenu megamenu">
                                  <li>
                                      <ul>
                                          <li><a href="<?php echo base_url(); ?>/index.php/Employee/create">Add New</a></li>
                                          <li><a href="<?php echo base_url(); ?>/index.php/Employee/list_all">View All</a></li>
                                          <li><a href="<?php echo base_url(); ?>/index.php/Employee/editEmp">Edit / Delete</a></li>
                                      </ul>
                                  </li>
                                  <li>
                                      <ul>
                                          <li><a href="<?php echo base_url(); ?>/index.php/Employee/generate">Generate Pay Slip</a></li>
                                          <li><a href="<?php echo base_url(); ?>/index.php/Employee/pay">Custom Pay Slip</a></li>

                                      </ul>
                                  </li>

                              </ul>
                          </li>

                          <li class="has-submenu">
                              <a href="#"><i class="zmdi zmdi-collection-item"></i><span> Attendence </span> </a>
                              <ul class="submenu">
                                  <li><a href="<?php echo base_url();?>/index.php/Attendence">Mark Attendence</a></li>
                                  <!-- <li><a href="page-starter.html">Manage Attendence</a></li> -->
                              </ul>
                          </li>

                          <li class="has-submenu">
                              <a href="#"><i class="zmdi zmdi-collection-text"></i><span> Clients </span> </a>
                              <ul class="submenu megamenu">
                                  <li>
                                      <ul>
                                          <li><a href="<?php echo base_url();?>/index.php/Clients/index">Add New</a></li>
                                          <li><a href="<?php echo base_url();?>/index.php/Clients/list_all">View All</a></li>
                                          <li><a href="<?php echo base_url();?>/index.php/Clients/editDelete">Edit / Delete</a></li>
                                      </ul>
                                  </li>
                                  <li>
                                      <ul>

                                          <li><a href="<?php echo base_url();?>/index.php/Clients/invoice">New Invoice</a></li>
                                          <li><a href="<?php echo base_url();?>/index.php/Clients/Manageinvoice">Manage Invoice</a></li>
                                      </ul>
                                  </li>

                              </ul>
                            <li>
                              <ul class="submenu">
                                  <li><a href="form-elements.html">Add New</a></li>
                                  <li><a href="form-advanced.html">View All</a></li>
                                  <li><a href="form-validation.html">Edit / Delete</a></li>
                              </ul>
                          </li>

                          </li>


                          <li class="has-submenu ">
                              <a href="#"><i class="zmdi zmdi-layers"></i><span>Assign Employees </span> </a>
                              <ul class="submenu megamenu">
                                  <li>
                                      <ul>
                                          <li><a href="<?php echo base_url();?>/index.php/Employee/dashview">Assign New</a></li>
                                          <li><a href="<?php echo base_url();?>/index.php/Employee/lassign">List Assigned</a></li>
                                          <!-- <li><a href="extras-taskboard.html">Manage</a></li> -->
                                      </ul>
                                  </li>

                              </ul>
                          </li>

                          <li class="has-submenu last-elements">
                              <a href="<?php echo base_url();?>/index.php/Settings"><i class="zmdi zmdi-view-list"></i> <span> Settings </span> </a>

                          </li>




                      </ul>
                      <!-- End navigation menu  -->
                  </div>
              </div>
          </div>
      </header>
      <!-- End Navigation Bar-->
