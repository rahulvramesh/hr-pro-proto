


      <div class="wrapper">
          <div class="container">
		  
		  
		  
		         <!-- Page-Title -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="btn-group pull-right m-t-15">
                            <button type="button" class="btn btn-custom dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <h4 class="page-title">Edit / Delete Invoice </h4>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            <div class="dropdown pull-right">
                                <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>

                            <h4 class="header-title m-t-0 m-b-30">View Invoice Details</h4>

                            <table width="699" class="table table-striped table-bordered" id="datatable-editable">
                                <thead>
                                    <tr>
                                        <th width="127">Employee Code</th>
                                        <th width="200">Employee Name</th>
                                        <th width="102">Detalis</th>
                                        <th width="39">Days Paid</th>
                                        <th width="84"><span class="form-group">
                                          <label for="emailAddress">Working Period</label>
                                        </span></th>
                                        <th width="84"><span class="form-group">
                                          <label for="pass1"></label>
                                          Total Earnings</span></th>
									    <th width="84">&nbsp;</th>
									    <th width="17">&nbsp;</th>
								    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td>Tiger Nixon</td>
                                        <td>System Architect</td>
                                        <td>Edinburgh</td>
                                        <td>61</td>
                                        <td>2011/04/25</td>
                                        <td>$320,800</td>
										<td><a href="<?php echo base_url();?>/index.php/Clients/viewinvoice">VIEW INVOICE</a> </td>
										<td><a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a> <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a> <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a> <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a> </td>
                                    </tr>
                                    <tr>
                                        <td>Garrett Winters</td>
                                        <td>Accountant</td>
                                        <td>Tokyo</td>
                                        <td>63</td>
                                        <td>2011/07/25</td>
                                        <td>$170,750</td>
                                        <td><a href="<?php echo base_url();?>/index.php/Clients/viewinvoice">VIEW INVOICE</a></td>
                                        <td><a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a> <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a> <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a> <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a> </td>
                                    </tr>
                                    <tr>
                                        <td>Ashton Cox</td>
                                        <td>Junior Technical Author</td>
                                        <td>San Francisco</td>
                                        <td>66</td>
                                        <td>2009/01/12</td>
                                        <td>$86,000</td> 
                                        <td><a href="<?php echo base_url();?>/index.php/Clients/viewinvoice">VIEW INVOICE</a></td>
                                        <td><a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a> <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a> <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a> <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a> </td>
                                    </tr>
                                    <tr>
                                        <td>Cedric Kelly</td>
                                        <td>Senior Javascript Developer</td>
                                        <td>Edinburgh</td>
                                        <td>22</td>
                                        <td>2012/03/29</td>
                                        <td>$433,060</td> 
                                        <td><a href="<?php echo base_url();?>/index.php/Clients/viewinvoice">VIEW INVOICE</a></td>
                                        <td><a href="#" class="hidden on-editing save-row"><i class="fa fa-save"></i></a> <a href="#" class="hidden on-editing cancel-row"><i class="fa fa-times"></i></a> <a href="#" class="on-default edit-row"><i class="fa fa-pencil"></i></a> <a href="#" class="on-default remove-row"><i class="fa fa-trash-o"></i></a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- end col -->
                </div>
                <!-- end row -->
				
				
				
				  <!-- Footer -->
              <footer class="footer text-right">
                  <div class="container">
                      <div class="row">
                          <div class="col-xs-6">
                              2016 � Alindal Technologies.
                          </div>
                          <div class="col-xs-6">
                              <ul class="pull-right list-inline m-b-0">
                                  <li>
                                      <a href="#">About</a>
                                  </li>
                                  <li>
                                      <a href="#">Help</a>
                                  </li>
                                  <li>
                                      <a href="#">Contact</a>
                                  </li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </footer>
              <!-- End Footer -->

          </div>
          <!-- end container -->


